package com.citi.traderestapi.controller;

import com.citi.traderestapi.model.Watch;
import com.citi.traderestapi.service.WatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class WatchController {
    @Autowired
    private WatchService watchService;

    @GetMapping(value="/watch", produces={"application/json","application/xml"})
    public ResponseEntity<List<Watch>> getWatchlist() {
        List<Watch> watch = watchService.getWatchlist();
        return ResponseEntity.ok().body(watch);
    }

    @PostMapping(value="/watch/{ticker}", consumes = {"application/json","application/xml"}, produces={"application/json","application/xml"})
    public ResponseEntity addWatchList(@PathVariable String ticker) {
        watchService.addToWatchList(ticker);
        return ResponseEntity.ok().body("Successfully added to watchlist");
    }

    @DeleteMapping(value="/watch/{ticker}", produces={"application/json","application/xml"})
    public ResponseEntity deleteFromWatchList(@PathVariable String ticker) {
        watchService.deleteFromWatchList(ticker);
        return ResponseEntity.ok().body("Deleted " + ticker + " from watchlist");
    }

}
