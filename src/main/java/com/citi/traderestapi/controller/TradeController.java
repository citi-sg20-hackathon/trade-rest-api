package com.citi.traderestapi.controller;

import com.citi.traderestapi.service.TradeService;
import com.citi.traderestapi.model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TradeController {
    @Autowired
    private TradeService tradeService;

    @GetMapping(value="/trade", produces={"application/json","application/xml"})
    public ResponseEntity<List<Trade>> getTrades() {
        List<Trade> trades = tradeService.getTrades();
        return ResponseEntity.ok().body(trades);
    }

    @PostMapping(value="/trade", produces={"application/json","application/xml"})
    public ResponseEntity<Trade> createTrade(@RequestBody Trade trade) {
        try {
            Trade savedTrade = tradeService.createTrade(trade.getType(), trade.getTicker(), trade.getQuantity(), trade.getPrice());
            return ResponseEntity.ok().body(savedTrade);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
