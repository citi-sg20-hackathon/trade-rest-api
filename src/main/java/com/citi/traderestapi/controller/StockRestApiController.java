package com.citi.traderestapi.controller;

import com.citi.traderestapi.model.Quote;
import com.citi.traderestapi.service.StockApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class StockRestApiController {

    @Autowired
    StockApiService stockApiService;

    @GetMapping(value="returns/stock/{ticker}", produces={"application/json","application/xml"})
    public ResponseEntity<Double> dayReturn(@PathVariable String ticker) {
        Double dayReturn = stockApiService.getDayReturn(ticker);
        return ResponseEntity.ok().body(dayReturn);
    }

    @GetMapping(value="price/stock/{ticker}", produces={"application/json","application/xml"})
    public ResponseEntity<Quote> getPrice(@PathVariable String ticker) {
        Quote quote = stockApiService.getQuote(ticker);
        return ResponseEntity.ok().body(quote);
    }
}