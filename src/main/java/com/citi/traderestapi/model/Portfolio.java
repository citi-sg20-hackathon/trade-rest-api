package com.citi.traderestapi.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Portfolio")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Portfolio {
    @Id
    private String portfolioId;
    private String imageUrl;

    public Portfolio(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
