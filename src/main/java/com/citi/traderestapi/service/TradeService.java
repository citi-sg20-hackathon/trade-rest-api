package com.citi.traderestapi.service;

import com.citi.traderestapi.dao.TradeMongoDao;
import com.citi.traderestapi.model.Trade;
import com.citi.traderestapi.model.TradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeService {
    @Autowired
    private TradeMongoDao tradeMongoDao;

    public Trade createTrade(TradeType type, String ticker, int quantity, double price) {
        Trade trade = new Trade(type, ticker, quantity, price);
        tradeMongoDao.save(trade);
        return trade;
    }

    public List<Trade> getTrades() {
        return tradeMongoDao.findAll();
    }
}
