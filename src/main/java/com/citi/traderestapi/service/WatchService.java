package com.citi.traderestapi.service;

import com.citi.traderestapi.dao.WatchDao;
import com.citi.traderestapi.model.Watch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WatchService {
    @Autowired
    private WatchDao watchDao;

    public void addToWatchList(String ticker) {
        Watch watch = new Watch(ticker);
        System.out.println(watch);
        watchDao.save(watch);
    }

    public void deleteFromWatchList(String ticker) {
        Watch watch = new Watch(ticker);
        watchDao.delete(watch);
    }


    public List<Watch> getWatchlist() {
        return watchDao.findAll();
    }
}
