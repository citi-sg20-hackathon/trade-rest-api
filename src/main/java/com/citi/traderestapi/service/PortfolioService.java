package com.citi.traderestapi.service;

import com.citi.traderestapi.dao.PortfolioDao;
import com.citi.traderestapi.model.Portfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortfolioService {
    @Autowired
    PortfolioDao portfolioDao;

    public Portfolio createPortfolio(String imageUrl) {
        try {
            Portfolio portfolio = new Portfolio(imageUrl);
            portfolioDao.save(portfolio);
            return portfolio;
        } catch (Exception e){
            System.out.println(e);
            return null;
        }
    }
    public void deletePortfolio(String portfolioId) {
        portfolioDao.deleteById(portfolioId);
    }
}
