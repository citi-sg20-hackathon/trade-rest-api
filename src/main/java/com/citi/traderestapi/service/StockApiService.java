package com.citi.traderestapi.service;

import com.citi.traderestapi.StockApiClient;
import com.citi.traderestapi.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StockApiService {

    @Autowired
    StockApiClient client;

    public double getDayReturn(String ticker) {
        Quote quote = client.getQuote(ticker);
        double current = quote.getCurrent();
        double open = quote.getOpen();
        return (current - open) / open;
    }

    public Quote getQuote(String ticker) {
        Quote quote = client.getQuote(ticker);
        System.out.println(quote);
        System.out.println(quote.getHigh());
        return quote;
    }
}
