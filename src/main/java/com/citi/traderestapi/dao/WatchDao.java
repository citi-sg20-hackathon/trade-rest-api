package com.citi.traderestapi.dao;

import com.citi.traderestapi.model.Watch;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WatchDao extends MongoRepository<Watch, String> {

}
