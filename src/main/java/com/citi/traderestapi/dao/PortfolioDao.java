package com.citi.traderestapi.dao;

import com.citi.traderestapi.model.Portfolio;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PortfolioDao extends MongoRepository<Portfolio, String> {
}
