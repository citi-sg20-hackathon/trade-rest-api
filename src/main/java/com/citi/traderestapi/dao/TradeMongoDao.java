package com.citi.traderestapi.dao;

import com.citi.traderestapi.model.Trade;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeMongoDao extends MongoRepository<Trade, String> {
}