package com.citi.traderestapi;

import com.citi.traderestapi.model.Quote;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StockApiClient {
    @Value("${spring.datasource.token}")
    private String token;
    @Value("${spring.datasource.baseurl}")
    private String baseAddress;

    public Quote getQuote(String ticker) {
        try {
            RestTemplate template = new RestTemplate();
            String url = baseAddress + "?symbol=" + ticker.toUpperCase() + "&token=" + token;
            System.out.println(url);
            Quote quote = template.getForObject(url, Quote.class);
            System.out.println(quote);
            return quote;
        } catch(Exception e) {
            System.out.println(e);
            return null;
        }
    }
}